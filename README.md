Kingpin map editor support
==========================

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Kingpin.

This gamepack is based on the game pack provided by Kingpin:

- http://download.kingpin.info/kingpin/editing/maps/map_editors/NetRadiant/addon/Kingpinpack.zip

This repository is temporary and may be scrapped over with another one, one day, or importing stuff from various places.

This repository may be ported to [mkeditorpacks](https://github.com/Unvanquished/unvanquished-mapeditor-support/tree/master/mkeditorpacks) to support more editors.

Quote from initial `readme.txt` file:

FREDZ v1.0

- v1.1: Updated def file.
- v1.0: First version